#!/usr/bin/env bash

# 设置颜色变量
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
plain='\033[0m'

# DNS 端口
dns_port=62580

# 检查是否为 root 用户
if [[ $EUID -ne 0 ]]; then
    echo -e "[${red}错误${plain}] 请使用 root 用户来执行脚本！"
    exit 1
fi

# 获取公网 IP
get_ip() {
    local IP=$(ip addr | grep 'inet ' | awk '{print $2}' | cut -d/ -f1 | egrep -v "^192\.168|^172\.1[6-9]\.|^172\.2[0-9]\.|^172\.3[0-2]\.|^10\.|^127\.|^255\.|^0\." | head -n 1)
    [ -z "${IP}" ] && IP=$(wget -qO- -t1 -T2 ipv4.icanhazip.com)
    [ -z "${IP}" ] && IP=$(wget -qO- -t1 -T2 ipinfo.io/ip)
    echo "${IP}"
}

# 检查 IP 格式
check_ip() {
    local ip=$1
    local stat=1

    if [[ $ip =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        if [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]; then
            stat=0
        fi
    fi
    if [[ $stat -ne 0 ]]; then
        echo -e "[${red}错误${plain}] IP 格式错误！"
    fi
    return $stat
}

# 下载文件
download() {
    local filename=$2
    echo -e "[${green}信息${plain}] 正在下载 ${filename}..."
    wget --no-check-certificate -q -O "$2" "$1"
    if [[ $? -ne 0 || ! -s "$2" ]]; then
        echo -e "[${red}错误${plain}] 下载 ${filename} 失败！"
        exit 1
    fi
}

# 安装依赖
install_dependencies() {
    echo -e "[${green}信息${plain}] 安装依赖软件..."
    apt-get update -y >/dev/null 2>&1
    apt-get install -y wget curl net-tools make gcc g++ pkg-config nettle-dev gettext libidn11-dev libidn2-dev libnetfilter-conntrack-dev libdbus-1-dev libssl-dev libev-dev libpcre3-dev libudns-dev >/dev/null 2>&1
    # 如果安装依赖软件失败，尝试更换源再次安装
    if [[ $? -ne 0 ]]; then
        echo -e "[${yellow}警告${plain}] 安装依赖软件失败，尝试更换源再次安装..."
        exit 1
    fi
}

# 安装 Dnsmasq
install_dnsmasq() {
    echo -e "[${green}信息${plain}] 安装 Dnsmasq..."
    apt-get install -y dnsmasq >/dev/null 2>&1
    # 停止系统自带的 dnsmasq，防止端口冲突
    systemctl stop dnsmasq >/dev/null 2>&1

    # 检查端口是否被占用
    if netstat -tulpen | grep ":${dns_port} " >/dev/null; then
        echo -e "[${red}错误${plain}] 必须的端口 ${dns_port} 已被占用"
        exit 1
    fi

    # 下载并编译最新版本的 dnsmasq
    cd /tmp/
    download "https://thekelleys.org.uk/dnsmasq/dnsmasq-2.90.tar.gz" "dnsmasq.tar.gz"
    tar xzf dnsmasq.tar.gz
    cd dnsmasq-2.90
    echo -e "[${green}信息${plain}] 正在编译 Dnsmasq..."
    make all-i18n COPTS='-DHAVE_DNSSEC -DHAVE_IDN -DHAVE_CONNTRACK -DHAVE_DBUS' >/dev/null 2>&1
    if [[ $? -ne 0 ]]; then
        echo -e "[${red}错误${plain}] 编译 Dnsmasq 失败！"
        exit 1
    fi
    make install >/dev/null 2>&1
    if [[ $? -ne 0 ]]; then
        echo -e "[${red}错误${plain}] 安装 Dnsmasq 失败！"
        exit 1
    fi

    # 配置 Dnsmasq
    download "https://gitlab.com/x-x/dns-proxy/-/raw/main/dnsmasq.conf" "/etc/dnsmasq.d/custom_netflix.conf"
    download "https://gitlab.com/x-x/dns-proxy/-/raw/main/dnsmasq.txt" "/tmp/proxy-domains.txt"
    local ip=${public_ip}

    # 处理域名列表，忽略注释和空行
    while IFS= read -r domain || [[ -n "$domain" ]]; do
        [[ -z "$domain" ]] && continue
        [[ "$domain" =~ ^# ]] && continue
        echo "address=/${domain}/${ip}" >>/etc/dnsmasq.d/custom_netflix.conf
    done </tmp/proxy-domains.txt

    # 设置 dnsmasq 监听端口为 dns_port
    echo "port=${dns_port}" >>/etc/dnsmasq.conf

    # 确保包含 dnsmasq.d 目录的配置
    if ! grep -q "conf-dir=/etc/dnsmasq.d" /etc/dnsmasq.conf; then
        echo "conf-dir=/etc/dnsmasq.d" >>/etc/dnsmasq.conf
    fi

    # 启动 dnsmasq 服务
    systemctl enable dnsmasq >/dev/null 2>&1
    systemctl restart dnsmasq >/dev/null 2>&1
    echo -e "[${green}信息${plain}] Dnsmasq 安装完成！"
}

# 安装 SNI Proxy
install_sniproxy() {
    echo -e "[${green}信息${plain}] 安装 SNI Proxy..."
    # 模式选择逻辑开始
    mode="ipv6_first"
    mode_option="2" # 默认选项

    echo "请选择 mode 类型："
    echo "1. mode ipv4_first"
    echo "2. mode ipv6_first"
    echo "默认为 ipv6_first，10秒后自动选择。"

    # 倒计时动画
    countdown=10
    while [ $countdown -gt 0 ]; do
        printf "\r在 %2d 秒内选择模式: " $countdown
        # 动画效果
        case $((countdown % 4)) in
        0) echo -n "/" ;;
        1) echo -n "-" ;;
        2) echo -n "\\" ;;
        3) echo -n "|" ;;
        esac
        read -t 1 -n 1 key
        if [ $? = 0 ]; then
            echo ""
            mode_option=$key
            break
        fi
        countdown=$((countdown - 1))
    done

    if [ -z "$key" ]; then
        echo -e "\n未检测到输入，使用默认模式: ipv6_first"
    else
        if [ "$mode_option" = "1" ]; then
            mode="ipv4_first"
            echo "选择了模式: ipv4_first"
        elif [ "$mode_option" = "2" ]; then
            mode="ipv6_first"
            echo "选择了模式: ipv6_first"
        else
            echo "输入无效，使用默认模式: ipv6_first"
        fi
    fi
    # 模式选择逻辑结束
    # 检查需要的端口是否被占用
    for port in 80 443; do
        if netstat -tulpen | grep ":${port} " >/dev/null; then
            echo -e "[${red}错误${plain}] 必须的端口 ${port} 已被占用"
            exit 1
        fi
    done

    cd /tmp/
    # 下载 sniproxy 的 deb 包
    local arch=$(dpkg --print-architecture)
    if [[ "${arch}" == "amd64" ]]; then
        download "https://gitlab.com/x-x/dns-proxy/-/raw/main/sniproxy/sniproxy_0.6.1_amd64.deb" "sniproxy.deb"
    else
        echo -e "[${red}错误${plain}] 暂不支持 ${arch} 架构的 sniproxy 安装"
        exit 1
    fi
    dpkg -i sniproxy.deb >/dev/null 2>&1
    if [[ $? -ne 0 ]]; then
        echo -e "[${red}错误${plain}] 安装 sniproxy 失败！"
        exit 1
    fi

    # 配置 SNI Proxy
    download "https://gitlab.com/x-x/dns-proxy/-/raw/main/sniproxy.conf" "/etc/sniproxy.conf"

    # 修改 sniproxy.conf，根据选择的模式设置 resolver 中的 mode
    sed -i "s/mode .*/mode ${mode};/" /etc/sniproxy.conf

    download "https://gitlab.com/x-x/dns-proxy/-/raw/main/dnsmasq.txt" "/tmp/sniproxy-domains.txt"

    # 处理域名列表，忽略注释和空行
    {
        echo "table {"
        while IFS= read -r domain || [[ -n "$domain" ]]; do
            [[ -z "$domain" ]] && continue
            [[ "$domain" =~ ^# ]] && continue
            # 对域名进行正则表达式转换
            escaped_domain=$(echo "$domain" | sed 's/\./\\./g')
            echo "    .*${escaped_domain}\$ *"
        done </tmp/sniproxy-domains.txt
        echo "}"
    } >>/etc/sniproxy.conf

    # 下载 sniproxy 的 systemd 服务文件
    download "https://gitlab.com/x-x/dns-proxy/-/raw/main/sniproxy.service" "/etc/systemd/system/sniproxy.service"

    # 启动 sniproxy 服务
    systemctl daemon-reload >/dev/null 2>&1
    systemctl enable sniproxy >/dev/null 2>&1
    systemctl restart sniproxy >/dev/null 2>&1
    apt-mark hold sniproxy >/dev/null 2>&1

    echo -e "[${green}信息${plain}] SNI Proxy 安装完成！"
}

# 卸载 Dnsmasq
uninstall_dnsmasq() {
    echo -e "[${green}信息${plain}] 卸载 Dnsmasq..."
    systemctl stop dnsmasq >/dev/null 2>&1
    apt-get remove --purge -y dnsmasq >/dev/null 2>&1
    rm -f /usr/local/sbin/dnsmasq
    rm -f /etc/dnsmasq.conf
    rm -rf /etc/dnsmasq.d
    echo -e "[${green}信息${plain}] Dnsmasq 卸载完成！"
}

# 卸载 SNI Proxy
uninstall_sniproxy() {
    echo -e "[${green}信息${plain}] 卸载 SNI Proxy..."
    systemctl stop sniproxy >/dev/null 2>&1
    systemctl disable sniproxy >/dev/null 2>&1
    apt-get remove --purge -y sniproxy >/dev/null 2>&1
    rm -f /etc/sniproxy.conf
    rm -f /etc/systemd/system/sniproxy.service
    echo -e "[${green}信息${plain}] SNI Proxy 卸载完成！"
}

# 显示帮助信息
show_help() {
    echo -e "${yellow}Dnsmasq + SNI Proxy 自助安装脚本${plain}"
    echo -e "支持系统: Debian 8+ / Ubuntu 16+"
    echo ""
    echo "用法：bash $0 [参数]"
    echo ""
    echo "参数："
    echo "  -i, --install             安装 Dnsmasq 和 SNI Proxy"
    echo "  -id, --installdnsmasq     仅安装 Dnsmasq"
    echo "  -is, --installsniproxy    仅安装 SNI Proxy"
    echo "  -u, --uninstall           卸载 Dnsmasq 和 SNI Proxy"
    echo "  -ud, --undnsmasq          卸载 Dnsmasq"
    echo "  -us, --unsniproxy         卸载 SNI Proxy"
    echo "  -h, --help                显示帮助信息"
    echo ""
}

# 主函数
main() {
    if [[ $# -ne 1 ]]; then
        show_help
        exit 1
    fi

    case $1 in
    -i | --install)
        if [[ ! -f /etc/debian_version ]]; then
            echo -e "[${red}错误${plain}] 本脚本仅支持 Debian/Ubuntu 系统！"
            exit 1
        fi
        public_ip=$(get_ip)
        if [[ -z "${public_ip}" ]]; then
            echo -e "[${red}错误${plain}] 无法获取服务器公网 IP！"
            exit 1
        fi
        install_dependencies
        install_dnsmasq
        install_sniproxy
        echo -e "${yellow}Dnsmasq + SNI Proxy 已完成安装！${plain}"
        echo -e "${yellow}将您的 DNS 更改为 ${public_ip}:${dns_port} 即可观看 Netflix 节目。${plain}"
        ;;
    -id | --installdnsmasq)
        if [[ ! -f /etc/debian_version ]]; then
            echo -e "[${red}错误${plain}] 本脚本仅支持 Debian/Ubuntu 系统！"
            exit 1
        fi
        public_ip=$(get_ip)
        if [[ -z "${public_ip}" ]]; then
            echo -e "[${red}错误${plain}] 无法获取服务器公网 IP！"
            exit 1
        fi
        install_dependencies
        install_dnsmasq
        echo -e "${yellow}Dnsmasq 已完成安装！${plain}"
        echo -e "${yellow}将您的 DNS 更改为 ${public_ip}:${dns_port} 即可观看 Netflix 节目。${plain}"
        ;;
    -is | --installsniproxy)
        if [[ ! -f /etc/debian_version ]]; then
            echo -e "[${red}错误${plain}] 本脚本仅支持 Debian/Ubuntu 系统！"
            exit 1
        fi
        public_ip=$(get_ip)
        if [[ -z "${public_ip}" ]]; then
            echo -e "[${red}错误${plain}] 无法获取服务器公网 IP！"
            exit 1
        fi
        install_dependencies
        install_sniproxy
        echo -e "${yellow}SNI Proxy 已完成安装！${plain}"
        echo -e "${yellow}将 Netflix 的相关域名解析到 ${public_ip} 即可观看 Netflix 节目。${plain}"
        ;;
    -u | --uninstall)
        uninstall_dnsmasq
        uninstall_sniproxy
        ;;
    -ud | --undnsmasq)
        uninstall_dnsmasq
        ;;
    -us | --unsniproxy)
        uninstall_sniproxy
        ;;
    -h | --help)
        show_help
        ;;
    *)
        show_help
        ;;
    esac
}

main "$@"
