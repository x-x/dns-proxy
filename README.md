# Dnsmasq SNIproxy One-click Install

### 脚本说明：

- 原理简述：使用[Dnsmasq](http://thekelleys.org.uk/dnsmasq/doc.html)的 DNS 将网站解析劫持到[SNIproxy](https://github.com/dlundquist/sniproxy)反向代理的页面上。

- 用途：让无法观看流媒体的 VPS 可以观看（前提：VPS 中要有一个是能观看流媒体的）。

- 特性：脚本默认解锁`Netflix Hulu HBO`[等](https://github.com/myxuchangbin/dnsmasq_sniproxy_install/blob/master/proxy-domains.txt)，如需增删流媒体域名请编辑文件`/etc/dnsmasq.d/custom_netflix.conf`和`/etc/sniproxy.conf`

- 脚本支持系统：Debian8+, Ubuntu16+
  - 理论上支持上述系统及不限制虚拟化类型，如有问题请反馈
  - 如果脚本最后显示的 IP 和实际公网 IP 不符，请修改一下文件`/etc/sniproxy.conf`中的 IP 地址

### 脚本用法：

    bash dnsmasq_sniproxy.sh -h

### 普通安装：

```Bash
wget --no-check-certificate -O dnsmasq_sniproxy.sh https://gitlab.com/x-x/dns-proxy/-/raw/main/dnsmasq_sniproxy.sh && bash dnsmasq_sniproxy.sh -i
```

### 卸载方法：

```Bash
wget --no-check-certificate -O dnsmasq_sniproxy.sh https://gitlab.com/x-x/dns-proxy/-/raw/main/dnsmasq_sniproxy.sh && bash dnsmasq_sniproxy.sh -u
```

### 使用方法：

将代理 VPS 的 DNS 地址修改为这个主机的 IP 就可以了，如果不能用，记得只保留一个 DNS 地址试一下。

防止滥用，建议不要随意公布 IP 地址，或使用防火墙做好限制工作。

### 调试排错：

- 确认 sniproxy 有效运行

  查看 sniproxy 状态：`systemctl status sniproxy`

  如果 sniproxy 不在运行，检查一下是否有其他服务占用 80,443 端口，导致端口冲突，查看端口监听命令：`netstat -tlunp | grep 443`

- 确认防火墙放行 62580,80,443

  调试可直接关闭防火墙 `systemctl stop firewalld.service`

  阿里云/谷歌云/AWS 等运营商安全组端口同样需要放行
  可通过其他服务器 `telnet 1.2.3.4 62580` 进行测试

- 解析域名测试

  尝试用其他服务器配置完毕 dns 后，解析域名：nslookup netflix.com 判断 IP 是否是 NETFLIX 代理机器 IP
  如果不存在 nslookup 命令，centos 安装：`yum install -y bind-utils` ubuntu&debian 安装：`apt-get -y install dnsutils`

- systemd-resolve 服务占用 62580 端口解决方法
  使用`netstat -tlunp|grep 62580`发现 62580 端口被 systemd-resolved 占用了
  修改`/etc/systemd/resolved.conf`
  ```
  [Resolve]
  DNS=8.8.8.8 1.1.1.1 #取消注释，增加dns
  #FallbackDNS=
  #Domains=
  #LLMNR=no
  #MulticastDNS=no
  #DNSSEC=no
  #Cache=yes
  DNSStubListener=no  #取消注释，把yes改为no
  ```
  接着再执行以下命令，并重启 systemd-resolved
  ```
  ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
  systemctl restart systemd-resolved.service
  ```

---

**_本脚本仅限解锁流媒体使用_**
